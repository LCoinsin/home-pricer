class TransactionsController < ApplicationController
  def index
  @city = City.find_by(postal_code: params.require(:zip_code))
  if !@city
    fetch
  end
  end

  def fetch
    JSON.parse(Typhoeus.get("https://data.opendatasoft.com/api/records/1.0/search/?dataset=demande-de-valeurs-foncieres-agrege-a-la-transaction%40public&q=&facet=code_postal&refine.code_postal=#{params.require(:zip_code)}").response_body)['records'].each do |record|
    @city = City.find_by("postal_code = '" + params[:zip_code] + "'") || City.create(postal_code:  record['fields']['code_postal'], name:  record['fields']['commune'])
          tr = @city.transactions.find_or_create_by(record_id: record['recordid'])
          tr.update({
            transaction_date: record['fields']['date_mutation'],
            address: "#{record['fields']['no_voie']} #{record['fields']['type_de_voie']} #{record['fields']['voie']}, #{record['fields']['code_postal']} #{record['fields']['commune']}",
            estate_type: record['fields']['type_local'],
            total_area: record['fields']['surface_reelle_bati'],
            number_of_rooms: record['fields']['nombre_pieces_principales'],
            value: record['fields']['valeur_fonciere'],
          }
          )
  end; render :index
  end
end
