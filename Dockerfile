# https://hub.docker.com/_/ruby
FROM ruby:3.0.5

# Install dependencies
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

# Setup Ruby environment
WORKDIR /app
RUN gem install pg

COPY . /app 

RUN bundle install

# Expose port 3000 to the Docker host, so we can access it
# from the outside.
EXPOSE 3000

# Run the web service on container startup.
CMD ["bash", "entrypoint.sh"]